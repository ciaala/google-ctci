//
// Created by crypt on 01/10/18.
//

#include <cassert>
#include "ex_01_03.hxx"
string ex_01::urlify(string s1) {
  auto space = 0u;
  for (char c : s1) {
    if (c == ' ') {
      space++;
    }
  }
  string result;
  result.reserve(space * 2 + s1.size());
  assert(result.size() == 0);
  for (char c : s1) {
    if (c == ' ') {
      result.push_back('%');
      result.push_back('2');
      result.push_back('0');
    } else {
      result.push_back(c);
    }
  }
  assert(result.size() == space * 2 + s1.size());
  return result;
}
