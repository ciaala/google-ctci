//
// Created by crypt on 29/09/18.
//

#ifndef GOOGLE_CTCI_ex_01_06_HXX
#define GOOGLE_CTCI_ex_01_06_HXX
#include "base.hxx"
namespace ex_01 {
string compressString(const string &s);
}
#endif //GOOGLE_CTCI_ex_01_06_HXX
