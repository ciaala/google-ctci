//
// Created by crypt on 01/10/18.
//

#ifndef GOOGLE_CTCI_ex_01_04_HXX
#define GOOGLE_CTCI_ex_01_04_HXX
#include "base.hxx"
namespace ex_01 {

bool isStringPermutatedPalyndrome(string s);
}
#endif //GOOGLE_CTCI_ex_01_04_HXX
