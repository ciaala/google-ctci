//
// Created by crypt on 01/10/18.
//

#include <bitset>
#include "ex_01_01.hxx"
static const int CHARS = 256;
bool ex_01::isAllUniqueCharacters(string s) {

  if (s.size() < 2) {
    return true;
  }
  if (s.size() > CHARS) {
    return false;
  }
  bitset<CHARS> flags(0);
  for (unsigned char i : s) {
    if (!flags[i]) {
      flags[i] = true;
    } else {
      return false;
    }
  }
  return true;
}
