//
// Created by crypt on 26/09/18.
//

#ifndef GOOGLE_CTCI_EXERCISE_01_5_HXX
#define GOOGLE_CTCI_EXERCISE_01_5_HXX
#include "base.hxx"
namespace ex_01 {
bool areOneEditAway(string s1, string s2);
}
#endif //GOOGLE_CTCI_EXERCISE_01_5_HXX
