//
// Created by crypt on 01/10/18.
//

#include <bitset>
#include "ex_01_04.hxx"
static const int CHAR_SYMBOLS = 256;

bool ex_01::isStringPermutatedPalyndrome(string s) {
  bitset<CHAR_SYMBOLS> charflags;
  charflags.reset();
  for (char c: s) {
    charflags.flip((size_t) c);
  }
  uint32_t countBitSet = charflags.count();
  return countBitSet < 2;
}
