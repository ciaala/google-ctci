//
// Created by crypt on 29/09/18.
//

#include "ex_01_07.hxx"
#include <sstream>
#include <cassert>
namespace ex_02 {
Matrix::Matrix(uint32_t
               *matrix,
               const uint32_t n) :
    recentre(n * n / 2),
    matrix(matrix),
    n(n) {
}
void Matrix::set(int x, int y, uint32_t value) {
  matrix[cellIndex(x, y)] = value;
}
uint32_t Matrix::get(int x, int y) {
  return matrix[cellIndex(x, y)];
}

uint32_t Matrix::cellIndex(int x, int y) {
  int index = y * n + x + recentre;
  assert(index >= 0);
  assert(index < n * n);
  return index;
}

ostream &operator<<(ostream &os, Matrix &m) {
  int range = m.n / 2;
  for (int i = -range; i < range; i++) {
    os << '|';
    for (int j = -range; j < range; j++) {
      os << setw(2) << m.get(i, j) << " ";
    }
    os << '|' << endl;
  }
  os << endl;
  return os;
}

void matrixRotation(uint32_t *matrix,
                    uint32_t n,
                    orientation angle) {
  // nothing to rotate
  if (n <= 1) {
    return;
  }
  Matrix m(matrix, n);
  cout << m;
  for (
      int i = 0;
      i < n / 2; i++) {
    for (
        int j = 0;
        j < n / 2; j++) {
      uint32_t r1 = m.get(i, j);
      uint32_t r4 = m.get(-j, i);
      uint32_t r3 = m.get(-i, -j);
      uint32_t r2 = m.get(j, -i);
      if (angle == POSITIVE) {
        m.set(i, j, r2);
        m.set(-j, i, r1);
        m.set(-i, -j, r4);
        m.set(j, -i, r3);
      } else {
        m.set(i, j, r4);
        m.set(-j, i, r3);
        m.set(-i, -j, r2);
        m.set(j, -i, r1);
      }
    }
  }
  cout << m;
}
}

namespace ex_01 {

uint32_t indexQ2(uint32_t x, uint32_t y, uint32_t n) {
  return y * n + x;
}
uint32_t indexQ1(uint32_t x, uint32_t y, uint32_t n) {
  uint32_t xp = n - 1 - y;
  uint32_t yp = x;
  return yp * n + xp;
}
uint32_t indexQ4(uint32_t x, uint32_t y, uint32_t n) {
  uint32_t xp = n - 1 - x;
  uint32_t yp = n - 1 - y;
  return yp * n + xp;
}
uint32_t indexQ3(uint32_t x, uint32_t y, uint32_t n) {
  uint32_t xp = y;
  uint32_t yp = n - 1 - x;
  return yp * n + xp;
}

void matrixRotation_take2(uint32_t *matrix,
                          uint32_t n,
                          orientation angle) {
  print_matrix(matrix, n, n);
  const auto half_n = n / 2;
  for (uint32_t i = 0; i < half_n; i++) {
    for (uint32_t j = 0; j < half_n; j++) {
      const auto r1 = matrix[indexQ1(i, j, n)];
      const auto r2 = matrix[indexQ2(i, j, n)];
      const auto r3 = matrix[indexQ3(i, j, n)];
      const auto r4 = matrix[indexQ4(i, j, n)];
      if (angle == POSITIVE) {
        matrix[indexQ1(i, j, n)] = r2;
        matrix[indexQ2(i, j, n)] = r3;
        matrix[indexQ3(i, j, n)] = r4;
        matrix[indexQ4(i, j, n)] = r1;
      } else {
        matrix[indexQ1(i, j, n)] = r4;
        matrix[indexQ2(i, j, n)] = r1;
        matrix[indexQ3(i, j, n)] = r2;
        matrix[indexQ4(i, j, n)] = r3;
      }
      print_matrix(matrix, n, n);
    }
  }
  print_matrix(matrix, n, n);
}

void matrixRotation(uint32_t *matrix,
                    uint32_t n,
                    orientation angle) {
  if (matrix == nullptr || n < 2) {
    return;
  }
  const auto half_n = n / 2;
  print_matrix(matrix, n, n);
  for (auto layer = 0u; layer < half_n; ++layer) {
    auto first = layer;
    auto last = n - 1 - layer;
    for (auto i = first; i < last; i++) {
      auto offset = i - first;
      // top
      auto top = &matrix[n * first + i];
      auto left = &matrix[n * (last - offset) + first];
      auto bottom = &matrix[n * last + (last - offset)];
      auto right = &matrix[n * i + last ];

      auto tmp = *top;

      if (angle == POSITIVE) {
        *top = *left;
        *left = *bottom;
        *bottom = *right;
        *right = tmp;
      } else {
        *top = *right;
        *right = *bottom;
        *bottom = *left;
        *left = tmp;
      }
    }
    print_matrix(matrix, n, n);
  }
}
}