//
// Created by crypt on 01/10/18.
//

#ifndef GOOGLE_CTCI_ex_01_09_HXX
#define GOOGLE_CTCI_ex_01_09_HXX
#include "base.hxx"
namespace ex_01 {
bool isStringRotation(string s1, string rotation);
}
#endif //GOOGLE_CTCI_ex_01_09_HXX
