//
// Created by crypt on 01/10/18.
//

#include "ex_01_02.hxx"
const static uint32_t CHAR_COUNT = 256;

bool ex_01::arePermutation(string s1, string s2) {
  uint32_t symbols[CHAR_COUNT] = {0};
  if (s1.size() != s2.size()) {
    return false;
  }

  for (int i = 0; i < s1.size(); ++i) {
    symbols[s1.at(i)]++;
    symbols[s2.at(i)]--;

  }

  for (unsigned int symbol : symbols) {
    if (symbol != 0) {
      return false;
    }
  }
  return true;
}
