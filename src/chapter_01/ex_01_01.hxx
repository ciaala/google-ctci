//
// Created by crypt on 01/10/18.
//

#ifndef GOOGLE_CTCI_ex_01_01_HXX
#define GOOGLE_CTCI_ex_01_01_HXX
#include "base.hxx"
namespace ex_01 {
bool isAllUniqueCharacters(string s);
}
#endif //GOOGLE_CTCI_ex_01_01_HXX
