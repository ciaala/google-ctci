//
// Created by crypt on 01/10/18.
//

#include "ex_01_09.hxx"
bool isSubstringOf(string s1,
                   string s2) {

  auto result = s1.find(s2) != string::npos;
  cout << s1 << " find " << s2 << " -> " << result << endl;
  return result;
}
bool ex_01::isStringRotation(string s1,
                             string rotation) {
  if (s1.size() != rotation.size()) {
    return false;
  }
  if (s1.empty()) {
    return true;
  }
  for (uint32_t i = 0; i < rotation.size(); i++) {
    if (isSubstringOf(s1, rotation.substr(i))
        && isSubstringOf(s1, rotation.substr(0, i))) {
      return true;
    }
  }
  return false;
}
