//
// Created by crypt on 29/09/18.
//

#include "ex_01_06.hxx"
#include <sstream>
string ex_01::compressString(const string &s) {
  stringstream ss;
  const auto size = s.size();
  if (size < 2) {
    return s;
  }

  char c = c = s.at(0);
  uint32_t count = 1ul;
  ss << c;

  for (uint32_t idx = 1ul; idx < size; idx++) {
    if (c == s.at(idx)) {
      count++;
    } else {
      ss << count;
      count = 1;
      c = s.at(idx);
      ss << c;
    }
  }
  ss << count;
  auto compressed = ss.str();
  return compressed.size() < s.size() ? compressed : s;
}
