//
// Created by crypt on 01/10/18.
//

#include "ex_01_08.hxx"
void ex_01::zeroMatrix(uint32_t *matrix,
                       uint32_t width,
                       uint32_t height) {

  bool columns[width] = {false};
  bool rows[height] = {false};
  print_matrix(matrix, width, height);
  for (auto i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      if (matrix[j * width + i] == 0) {
        columns[i] = true;
        rows[j] = true;
      }
    }
  }
  for (auto i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      if (columns[i] || rows[j]) {
        matrix[j * width + i] = 0;
      }
    }
  }
  print_matrix(matrix, width, height);

}
