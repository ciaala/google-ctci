//
// Created by crypt on 01/10/18.
//

#ifndef GOOGLE_CTCI_ex_01_08_HXX
#define GOOGLE_CTCI_ex_01_08_HXX
#include "base.hxx"
namespace ex_01 {
void zeroMatrix(uint32_t *matrix, uint32_t width, uint32_t height);
}
#endif //GOOGLE_CTCI_ex_01_08_HXX
