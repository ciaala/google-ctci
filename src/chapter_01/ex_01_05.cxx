//
// Created by crypt on 26/09/18.
//

#include "ex_01_05.hxx"

namespace ex_01 {
bool areOneEditAway(string s1, string s2) {
  const auto last_index_01 = s1.size() - 1;
  const auto last_index_2 = s2.size() - 1;
  if( last_index_01 == -1 && last_index_2 == -1)
    return true;
  ulong size_diff = last_index_01 < last_index_2 ? last_index_2 - last_index_01 : last_index_01 - last_index_2;
  if (size_diff > 1)
    return false;
  auto min_size = last_index_01 < last_index_2 ? last_index_01 : last_index_2;

  ulong idx1 = 0;
  while (idx1 < min_size && s1.at(idx1) == s2.at(idx1) ) { idx1++; };

  if (idx1 == min_size)
    return true;

  ulong idx2 = idx1;
  if (last_index_01  <  last_index_2 ) {
    idx2++;
  } else if (last_index_01 > last_index_2 ) {
    idx1++;
  } else if (last_index_01 == last_index_2) {
    idx1++;
    idx2++;
  } else {
    return false;
  }
  while (idx1 <= last_index_01 && idx2 <= last_index_2 && s1.at(idx1++) == s2.at(idx2++) );
  return idx1 > last_index_01 && idx2 > last_index_2;
}
}