//
// Created by crypt on 29/09/18.
//

#ifndef GOOGLE_CTCI_ex_01_06_HXX
#define GOOGLE_CTCI_ex_01_06_HXX
#include "base.hxx"
namespace ex_02 {

enum orientation {
  POSITIVE,
  NEGATIVE
};
class Matrix {
 private:
  const int recentre;
  uint32_t *matrix;
  const uint32_t n;
 public:
  Matrix(uint32_t *matrix,
         const uint32_t n);
  uint32_t cellIndex(int x, int y);
  uint32_t get(int x, int y);
  void set(int x, int y, uint32_t value);
  friend ostream &operator<<(ostream &os, Matrix &m);

};
/**
 * Rotate a square matrix of size n
 * @param matrix
 * @param n
 */
void matrixRotation(uint32_t *matrix, const uint32_t n, orientation angle);
}

namespace ex_01 {

enum orientation {
  POSITIVE,
  NEGATIVE
};
uint32_t indexQ1(uint32_t x, uint32_t y, uint32_t n);
uint32_t indexQ2(uint32_t x, uint32_t y, uint32_t n);
uint32_t indexQ3(uint32_t x, uint32_t y, uint32_t n);
uint32_t indexQ4(uint32_t x, uint32_t y, uint32_t n); 

void matrixRotation(uint32_t *matrix, const uint32_t n, orientation angle);
}
#endif //GOOGLE_CTCI_ex_01_06_HXX
