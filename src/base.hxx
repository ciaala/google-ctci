//
// Created by crypt on 26/09/18.
//

#ifndef GOOGLE_CTCI_BASE_HXX
#define GOOGLE_CTCI_BASE_HXX

#include <string>
#include <iostream>
#include <iomanip>
using namespace std;
void print_matrix(uint32_t *matrix,
                  uint32_t w,
                  uint32_t h);
#endif //GOOGLE_CTCI_BASE_HXX
