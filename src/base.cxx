//
// Created by crypt on 01/10/18.
//
#include "base.hxx"

void print_matrix(uint32_t *matrix, uint32_t w, uint32_t h) {
  cout << endl;

  for (int j = 0; j < h; ++j) {
    cout << '|' << ' ';
    for (int i = 0; i < w; ++i) {
      cout << setw(2) << setfill(' ') << matrix[j * w + i] << ' ';
    }
    cout << '|' << endl;
  }
  cout << endl;
}
