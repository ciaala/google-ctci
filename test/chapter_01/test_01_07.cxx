//
// Created by crypt on 29/09/18.
//

#include "gtest/gtest.h"
#include "chapter_01/ex_01_07.hxx"

namespace {
using ex_01::orientation;
TEST(matrix_rotation, empty) {
  uint32_t m[] = {99ul};
  ex_01::matrixRotation(&m[0], 0, orientation::POSITIVE);
  EXPECT_EQ(m[0], m[0]);
}
TEST(matrix_rotation, with_n_1) {
  uint32_t m[] = {99ul};
  ex_01::matrixRotation(&m[0], 1, orientation::POSITIVE);
  EXPECT_EQ(m[0], m[0]);
}
TEST(matrix_rotation, with_n_2) {
  uint32_t m[] = {0, 1, 2, 3};
  uint32_t ref[] = {2, 0, 3, 1};
  uint32_t n = 2;
  ex_01::matrixRotation(&m[0], n, orientation::POSITIVE);
  for (int i = 0; i < n * n; i++) {
    EXPECT_EQ(m[i], ref[i]);
  }

}
TEST(matrix_rotation, with_n_3) {
  uint32_t m[] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
  uint32_t ref[] = {6, 3, 0, 7, 4, 1, 8, 5, 2};
  uint32_t n = 3;
  ex_01::matrixRotation(&m[0], n, orientation::POSITIVE);
  for (int i = 0; i < n * n; i++) {
    EXPECT_EQ(m[i], ref[i]);
  }

}

TEST(matrix_rotation, with_n_4) {
  uint32_t m[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF};
  uint32_t ref[] = {0xC, 8, 4, 0, 0xD, 9, 5, 1, 0xE, 0xA, 6, 2, 0xF, 0xB, 7, 3};
  uint32_t n = 4;
  ex_01::matrixRotation(&m[0], n, orientation::POSITIVE);
  for (int i = 0; i < n * n; i++) {
    EXPECT_EQ(m[i], ref[i]);
  }
}

TEST(matrix_rotation, with_n_5) {
  uint32_t m[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 , 16 , 17, 18, 19, 20, 21, 23, 23, 24};
  uint32_t ref[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 , 16 , 17, 18, 19, 20, 21, 23, 23, 24};
  uint32_t n = 5;
  ex_01::matrixRotation(&m[0], n, orientation::POSITIVE);
  ex_01::matrixRotation(&m[0], n, orientation::NEGATIVE);
  for (int i = 0; i < n * n; i++) {
    EXPECT_EQ(m[i], ref[i]);
  }
}

TEST(indexCheck, matrix22__0_0__0_03__03_3__03_0) {
  EXPECT_EQ(ex_01::indexQ1(0, 0, 4), 3);
  EXPECT_EQ(ex_01::indexQ2(0, 0, 4), 0);
  EXPECT_EQ(ex_01::indexQ3(0, 0, 4), 12);
  EXPECT_EQ(ex_01::indexQ4(0, 0, 4), 15);
}
TEST(indexCheck, matrix22__01_1__02_1__02_2__01_2) {
  EXPECT_EQ(ex_01::indexQ1(1, 1, 4), 6);
  EXPECT_EQ(ex_01::indexQ2(1, 1, 4), 5);
  EXPECT_EQ(ex_01::indexQ3(1, 1, 4), 9);
  EXPECT_EQ(ex_01::indexQ4(1, 1, 4), 10);
}

TEST(indexCheck, matrix33__01_1__02_1__02_2__01_2) {
  int x = 1, y = 1, n = 3;
  EXPECT_EQ(ex_01::indexQ1(x, y, n), 4);
  EXPECT_EQ(ex_01::indexQ2(x, y, n), 4);
  EXPECT_EQ(ex_01::indexQ3(x, y, n), 4);
  EXPECT_EQ(ex_01::indexQ4(x, y, n), 4);
}
TEST(indexCheck, matrix33__0_01__02_1__02_2__01_2) {
  int x = 1, y = 0, n = 3;
  EXPECT_EQ(ex_01::indexQ1(x, y, n), 1);
  EXPECT_EQ(ex_01::indexQ2(x, y, n), 5);
  EXPECT_EQ(ex_01::indexQ3(x, y, n), 7);
  EXPECT_EQ(ex_01::indexQ4(x, y, n), 3);
}

TEST(indexCheck, matrix55__01_1__04_1__04_3__01_3) {
  EXPECT_EQ(ex_01::indexQ1(1, 1, 5), 8);
  EXPECT_EQ(ex_01::indexQ2(1, 1, 5), 6);
  EXPECT_EQ(ex_01::indexQ3(1, 1, 5), 16);
  EXPECT_EQ(ex_01::indexQ4(1, 1, 5), 18);
}

TEST(indexCheck, matrix55__0_01__03_0__05_3__01_3) {
  EXPECT_EQ(ex_01::indexQ1(0, 1, 5), 3);
  EXPECT_EQ(ex_01::indexQ2(0, 1, 5), 5);
  EXPECT_EQ(ex_01::indexQ3(0, 1, 5), 21);
  EXPECT_EQ(ex_01::indexQ4(0, 1, 5), 19);
}
TEST(indexCheck, matrix55__0_02__03_0__05_3__01_3) {
  EXPECT_EQ(ex_01::indexQ1(0, 1, 5), 3);
  EXPECT_EQ(ex_01::indexQ2(0, 1, 5), 5);
  EXPECT_EQ(ex_01::indexQ3(0, 1, 5), 21);
  EXPECT_EQ(ex_01::indexQ4(0, 1, 5), 19);
}
}