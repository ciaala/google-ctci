//
// Created by crypt on 29/09/18.
//

#include "gtest/gtest.h"
#include "chapter_01/ex_01_06.hxx"

namespace {
TEST(compressString, empty_string) {
  EXPECT_EQ(ex_01::compressString("").size(), 0);
}

TEST(compressString, single_character) {
  EXPECT_EQ(ex_01::compressString("a"), "a");
}

TEST(compressString, uncompressable_single_character) {
  EXPECT_EQ(ex_01::compressString("a"), "a");
}

TEST(compressString, uncompressable_double_character) {
  EXPECT_EQ(ex_01::compressString("aa"), "aa");
}
TEST(compressString, compressable_triple_character) {
  EXPECT_EQ(ex_01::compressString("aaa"), "a3");
}
TEST(compressString, uncompressable_aa_bb) {
  EXPECT_EQ(ex_01::compressString("aabb"), "aabb");
}

TEST(compressString, compressable_aaa_bb__a3_b2) {
  EXPECT_EQ(ex_01::compressString("aaabb"), "a3b2");
}

TEST(compressString, compressable_aaa_bb_ccc_a3_b2_c3) {
  EXPECT_EQ(ex_01::compressString("aaabbccc"), "a3b2c3");
}



TEST(compressString, uncompressable_aaaaabcdabcd) {
  EXPECT_EQ(ex_01::compressString("aaaaabcdabcd"), "aaaaabcdabcd");
}


TEST(compressString, compressable_aaaaaaaaaaabcdabcd_a11b1c1d1a1b1c1d1) {
  EXPECT_EQ(ex_01::compressString("aaaaaaaaaaabcdabcd"), "a11b1c1d1a1b1c1d1");
}

}