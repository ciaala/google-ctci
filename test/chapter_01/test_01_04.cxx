//
// Created by crypt on 01/10/18.
//
#include "gtest/gtest.h"
#include "chapter_01/ex_01_04.hxx"
namespace {
TEST(isStringPermutatedPalyndrome, empty) {
  EXPECT_TRUE(ex_01::isStringPermutatedPalyndrome(""));
}

TEST(isStringPermutatedPalyndrome, single_a) {
  EXPECT_TRUE(ex_01::isStringPermutatedPalyndrome("a"));
}
TEST(isStringPermutatedPalyndrome, string_acab) {
  EXPECT_FALSE(ex_01::isStringPermutatedPalyndrome("acab"));
}
TEST(isStringPermutatedPalyndrome, string_ab) {
  EXPECT_FALSE(ex_01::isStringPermutatedPalyndrome("ab"));
}

TEST(isStringPermutatedPalyndrome, string_abc) {
  EXPECT_FALSE(ex_01::isStringPermutatedPalyndrome("abc"));
}

TEST(isStringPermutatedPalyndrome, string_abb) {
  EXPECT_TRUE(ex_01::isStringPermutatedPalyndrome("abb"));
  EXPECT_TRUE(ex_01::isStringPermutatedPalyndrome("bba"));
  EXPECT_TRUE(ex_01::isStringPermutatedPalyndrome("bab"));
}

TEST(isStringPermutatedPalyndrome, string_abba) {
  EXPECT_TRUE(ex_01::isStringPermutatedPalyndrome("abba"));
  EXPECT_TRUE(ex_01::isStringPermutatedPalyndrome("bbaa"));
  EXPECT_TRUE(ex_01::isStringPermutatedPalyndrome("aabb"));
  EXPECT_TRUE(ex_01::isStringPermutatedPalyndrome("abba"));
}

TEST(isStringPermutatedPalyndrome, string_abba_gold) {
  EXPECT_FALSE(ex_01::isStringPermutatedPalyndrome("abbagold"));
  EXPECT_FALSE(ex_01::isStringPermutatedPalyndrome("bbaagold"));
  EXPECT_FALSE(ex_01::isStringPermutatedPalyndrome("aabbgold"));
  EXPECT_FALSE(ex_01::isStringPermutatedPalyndrome("abbagold"));
}

}