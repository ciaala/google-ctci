//
// Created by crypt on 01/10/18.
//
#include "gtest/gtest.h"
#include "chapter_01/ex_01_08.hxx"

namespace {
TEST(zeroMatrix, empty_matrix) {
  ex_01::zeroMatrix(nullptr, 0, 0);
}
TEST(zeroMatrix, single_element_not_zero) {
  uint32_t matrix[1] = {1u};
  ex_01::zeroMatrix(matrix, 1, 1);
  EXPECT_EQ(matrix[0], 1u);
}

TEST(zeroMatrix, 2_02_matrix_not_zero) {
  uint32_t matrix[4] = {1u, 1u, 1u, 1u};
  ex_01::zeroMatrix(matrix, 2, 2);
  EXPECT_EQ(matrix[0], 1u);
  EXPECT_EQ(matrix[1], 1u);
  EXPECT_EQ(matrix[2], 1u);
  EXPECT_EQ(matrix[3], 1u);
}

TEST(zeroMatrix, 1_04_matrix_not_zero) {
  uint32_t matrix[4] = {0u, 1u, 1u, 1u};
  ex_01::zeroMatrix(matrix, 1, 4);
  EXPECT_EQ(matrix[0], 0u);
  EXPECT_EQ(matrix[1], 0u);
  EXPECT_EQ(matrix[2], 0u);
  EXPECT_EQ(matrix[3], 0u);
}
TEST(zeroMatrix, 4_01_matrix_not_zero) {
  uint32_t matrix[4] = {0u, 1u, 1u, 1u};
  ex_01::zeroMatrix(matrix, 4, 1);
  EXPECT_EQ(matrix[0], 0u);
  EXPECT_EQ(matrix[1], 0u);
  EXPECT_EQ(matrix[2], 0u);
  EXPECT_EQ(matrix[3], 0u);
}
TEST(zeroMatrix, 2_02_matrix_zero) {
  uint32_t matrix[4] = {1u, 1u, 1u, 0u};
  ex_01::zeroMatrix(matrix, 2, 2);
  EXPECT_EQ(matrix[0], 1u);
  EXPECT_EQ(matrix[1], 0u);
  EXPECT_EQ(matrix[2], 0u);
  EXPECT_EQ(matrix[3], 0u);
}

TEST(zeroMatrix, 2_03_matrix_zero) {
  uint32_t matrix[6] = {1u, 1u, 1u, 0u, 1u, 0u};
  ex_01::zeroMatrix(matrix, 2, 3);
  EXPECT_EQ(matrix[0], 1u);
  EXPECT_EQ(matrix[1], 0u);
  EXPECT_EQ(matrix[2], 0u);
  EXPECT_EQ(matrix[3], 0u);
  EXPECT_EQ(matrix[4], 0u);
  EXPECT_EQ(matrix[5], 0u);
}
TEST(zeroMatrix, 3_02_matrix_zero) {
  uint32_t matrix[6] = {1u, 1u, 1u, 0u, 1u, 0u};
  ex_01::zeroMatrix(matrix, 3, 2);
  EXPECT_EQ(matrix[0], 0u);
  EXPECT_EQ(matrix[1], 1u);
  EXPECT_EQ(matrix[2], 0u);
  EXPECT_EQ(matrix[3], 0u);
  EXPECT_EQ(matrix[4], 0u);
  EXPECT_EQ(matrix[5], 0u);
}

TEST(zeroMatrix, 3_03_matrix_zero) {
  uint32_t matrix[9] = {0u, 1u, 2u, 3u, 4u, 5u, 6u, 7u, 8u};
  ex_01::zeroMatrix(matrix, 3, 3);
  EXPECT_EQ(matrix[0], 0u);
  EXPECT_EQ(matrix[1], 0u);
  EXPECT_EQ(matrix[2], 0u);
  EXPECT_EQ(matrix[3], 0u);
  EXPECT_EQ(matrix[4], 4u);
  EXPECT_EQ(matrix[5], 5u);
  EXPECT_EQ(matrix[6], 0u);
  EXPECT_EQ(matrix[7], 7u);
  EXPECT_EQ(matrix[8], 8u);
}


TEST(zeroMatrix, 3_03_matrix_central_zero) {
  uint32_t matrix[9] = {99u, 1u, 2u, 3u, 0u, 5u, 6u, 7u, 8u};
  ex_01::zeroMatrix(matrix, 3, 3);

  EXPECT_EQ(matrix[0], 99u);
  EXPECT_EQ(matrix[1], 0u);
  EXPECT_EQ(matrix[2], 2u);
  EXPECT_EQ(matrix[3], 0u);
  EXPECT_EQ(matrix[4], 0u);
  EXPECT_EQ(matrix[5], 0u);
  EXPECT_EQ(matrix[6], 6u);
  EXPECT_EQ(matrix[7], 0u);
  EXPECT_EQ(matrix[8], 8u);
}
}