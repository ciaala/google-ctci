//
// Created by crypt on 01/10/18.
//
#include "gtest/gtest.h"
#include "chapter_01/ex_01_09.hxx"
namespace {
TEST(isStringRotation, empty_strings_return_true) {
  EXPECT_TRUE(ex_01::isStringRotation("", ""));
}

TEST(isStringRotation, different_length_strings_return_false) {
  EXPECT_FALSE(ex_01::isStringRotation("abc", "abcdef"));
}

TEST(isStringRotation, repeated_pattern_strings_return_false) {
  EXPECT_FALSE(ex_01::isStringRotation("abc", "abcabc"));
}

TEST(isStringRotation, same_strings_return_true) {
  EXPECT_TRUE(ex_01::isStringRotation("abc", "abc"));
}
TEST(isStringRotation, rotated_strings_return_true) {
  cout << endl;
  EXPECT_TRUE(ex_01::isStringRotation("bca", "abc"));
  EXPECT_TRUE(ex_01::isStringRotation("cab", "abc"));
  EXPECT_TRUE(ex_01::isStringRotation("abc", "bca"));
  EXPECT_TRUE(ex_01::isStringRotation("abc", "cab"));
  EXPECT_TRUE(ex_01::isStringRotation("bca", "cab"));
}
TEST(isStringRotation, different_strings_return_false) {
  EXPECT_FALSE(ex_01::isStringRotation("abc", "def"));
  EXPECT_FALSE(ex_01::isStringRotation("abc", "abf"));
}
}