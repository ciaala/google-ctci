//
// Created by crypt on 01/10/18.
//
#include "gtest/gtest.h"
#include "chapter_01/ex_01_02.hxx"

namespace {
TEST(arePermutation, empty) {
  EXPECT_TRUE(ex_01::arePermutation("", ""));
}
TEST(arePermutation, single_character_same_string) {
  EXPECT_TRUE(ex_01::arePermutation("a", "a"));
}
TEST(arePermutation, 2_characters_same_string) {
  EXPECT_TRUE(ex_01::arePermutation("ab", "ab"));
}

TEST(arePermutation, 3_characters_same_string) {
  EXPECT_TRUE(ex_01::arePermutation("abc", "abc"));
}
TEST(arePermutation, 3_characters_permutated_string) {
  EXPECT_TRUE(ex_01::arePermutation("abc", "bac"));
}

TEST(arePermutation, 3_characters_not_permutated_string) {
  EXPECT_FALSE(ex_01::arePermutation("abc", "bad"));
}
}