//
// Created by crypt on 26/09/18.
//
#include "gtest/gtest.h"
#include "chapter_01/ex_01_05.hxx"

namespace {

TEST(areOneEditAway, empty_string) {
  EXPECT_TRUE(ex_01::areOneEditAway("", ""));
}

TEST(areOneEditAway, same_string) {
  EXPECT_TRUE(ex_01::areOneEditAway("pizza", "pizza"));
}
TEST(areOneEditAway, short_one_at_the_end_PIZZ) {
  EXPECT_TRUE(ex_01::areOneEditAway("pizza", "pizz"));
}
TEST(areOneEditAway, short_one_at_the_start_IZZA) {
  EXPECT_TRUE(ex_01::areOneEditAway("izza", "pizza"));
}
TEST(areOneEditAway, short_one_at_the_middle_PIZA) {
  EXPECT_TRUE(ex_01::areOneEditAway("pizza", "piza"));
}

TEST(areOneEditAway, one_changed_PAZZA) {
  EXPECT_TRUE(ex_01::areOneEditAway("pizza", "pazza"));
}

TEST(areOneEditAway, one_changed_at_the_end_PIZZO) {
  EXPECT_TRUE(ex_01::areOneEditAway("pizza", "pizzo"));
}

TEST(areOneEditAway, one_changed_at_the_start_NIZZA) {
  EXPECT_TRUE(ex_01::areOneEditAway("pizza", "nizza"));
}

TEST(areOneEditAway, too_many_at_the_end_PIZZAAA) {
  EXPECT_FALSE(ex_01::areOneEditAway("pizza", "pizzaaa"));
}

TEST(areOneEditAway, just_different_PASTA) {
  EXPECT_FALSE(ex_01::areOneEditAway("pizza", "pasta"));
}

TEST(areOneEditAway, short_at_the_start_end_IZZ) {
  EXPECT_FALSE(ex_01::areOneEditAway("pizza", "izz"));
}

TEST(areOneEditAway, one_empty_one_long_3) {
  EXPECT_FALSE(ex_01::areOneEditAway("", "abc"));
}

TEST(areOneEditAway, one_empty_one_long_1) {
  EXPECT_FALSE(ex_01::areOneEditAway("", "a"));
}

}