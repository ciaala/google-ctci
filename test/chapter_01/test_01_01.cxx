//
// Created by crypt on 01/10/18.
//
#include "gtest/gtest.h"
#include "chapter_01/ex_01_01.hxx"

namespace {
TEST(isAllUniqueCharacters, empty_string) {
  EXPECT_TRUE(ex_01::isAllUniqueCharacters(""));
}
TEST(isAllUniqueCharacters, single_character_string) {
  EXPECT_TRUE(ex_01::isAllUniqueCharacters("a"));
}
TEST(isAllUniqueCharacters, abc) {
  EXPECT_TRUE(ex_01::isAllUniqueCharacters("abc"));
}

TEST(isAllUniqueCharacters, duplicated) {
  EXPECT_FALSE(ex_01::isAllUniqueCharacters("aa"));
}

TEST(isAllUniqueCharacters, repeated) {
  EXPECT_FALSE(ex_01::isAllUniqueCharacters("aba"));
}

TEST(isAllUniqueCharacters, all_characters) {
  string a;
  for (unsigned char c = 0; c < 255; c++ ) {
    a += c;
  }
  EXPECT_TRUE(ex_01::isAllUniqueCharacters(a));
}


TEST(isAllUniqueCharacters, double_all_characters) {
  string a;
  for (unsigned char c = 0; c < 255; c++ ) {
    a += c;
  }
  for (unsigned char c = 0; c < 255; c++ ) {
    a += c;
  }
  EXPECT_FALSE(ex_01::isAllUniqueCharacters(a));
}

}