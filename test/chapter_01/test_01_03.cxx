//
// Created by crypt on 01/10/18.
//
#include "gtest/gtest.h"
#include "chapter_01/ex_01_03.hxx"
namespace {
TEST(urlify, empty) {
  EXPECT_EQ(ex_01::urlify(""), "");
}
TEST(urlify, single_space) {
  EXPECT_EQ(ex_01::urlify(" "), "%20");
}

TEST(urlify, hello_world) {
  EXPECT_EQ(ex_01::urlify("Hello, World!"), "Hello,%20World!");
}

TEST(urlify, repated_space) {
  EXPECT_EQ(ex_01::urlify("  "), "%20%20");
}
}