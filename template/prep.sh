#!/bin/sh

chapter=$1
chapter_X=chapter_$1
exercise=$2

identifier=${chapter}_${exercise}
ex_source=../src/${chapter_X}/ex_${identifier}.cxx
ex_header=../src/${chapter_X}/ex_${identifier}.hxx
test_source=../test/${chapter_X}/test_${identifier}.cxx
mkdir -p ../src/${chapter_X}
mkdir -p ../test/${chapter_X}
cp ex_X_Y.cxx ${ex_source}
cp ex_X_Y.hxx ${ex_header}
cp test_X_Y.cxx ${test_source}

for f in ${ex_source} ${ex_header} ${test_source}
do
    sed -i "s/_X_Y/_${identifier}/g" ${f};
    sed -i "s/chapter_X/${chapter_X}/g" ${f};
done

source_file=src\/${chapter_X}\/ex_${identifier}.cxx
header_file=src\/${chapter_X}\/ex_${identifier}.hxx
test_file=test\/${chapter_X}\/test_${identifier}.cxx

for f in ${source_file} ${header_file} ${test_file}
do
    if [[ ${f} = *"test"*  ]];
    then
        sed -i "s/\#TEST_EXTRA/${f}\n\t\t#TEST_EXTRA/g" ../CMakeLists.txt
    else
        sed -i "s/\#EXTRA/${f}\n\t\t#EXTRA/g" ../CMakeLists.txt
    fi
    git add ${f}
done


